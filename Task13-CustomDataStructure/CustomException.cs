﻿
using System;
using System.Collections.Generic;
using System.Text;
namespace Task13_CustomDataStructure
{
    class CustomException : Exception
    {
        public CustomException()
        {
        }
        public CustomException(string message) : base(message)
        {
        }
        public CustomException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}