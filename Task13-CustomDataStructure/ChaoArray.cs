﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13_CustomDataStructure
{
    class chaosArray<T>
    {
        private int size;
        T[] myArray;
        public chaosArray(int size)
        {
            this.Size = size;
            this.myArray = new T[size];
        }
        public int Size { get => size; set => size = value; }
        // AddNewItem is a method to add the item in random place (Index)
        public void AddNewItem(T input)
        {
            try
            {
                Random listOfNewItem = new Random();
                int randomIndex = listOfNewItem.Next(size);
                // Check if random index is free, if true add to array else throw expception.
                if (myArray[randomIndex].Equals(default(T)))// If random index is empty
                {
                    myArray[randomIndex] = input;
                    Console.WriteLine($"Item {input} is added in index {randomIndex }  " );
                }
                else
                {
                    throw new CustomException("This space is not free to add an item !");
                }
            }
            catch (CustomException ex)
            {
                
                Console.WriteLine(ex.Message);
            }
        }
        // RetriveRandomNumber is method for retriving a random item.
        public void RetriveRandomNumber()
        {
            try
            {
                Random indextList = new Random();
                int randomIndex = indextList.Next(size);
                // Check if random index is free, if true add to array else throw an expception
                if (myArray[randomIndex].Equals(default(T)))//If the random index is empty.
                {
                    throw new CustomException("This index is empty it is not possible to retrive please try again!");
                }
                else
                {
                    Console.WriteLine("************************Retrive ITEM**************************");

                    Console.WriteLine($"This is a random item {myArray[randomIndex]}  with index {randomIndex} has been retrived");
                }
            }
            catch (CustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public T[] GetAll()
        {
            return myArray;
        }
    }
}